#!/usr/bin/env bash
# Runs the npm build process to create React's build folder, then copies the
# resulting files into our build folder to be dockerized

cd ./build || exit 1
rm -rf site
mkdir site
cd site || exit 1

# We fork the build process here, because if we're building with docker, we
# don't want to force the user to install node or anything like that. If
# they want to run the dockerized versions, they shouldn't have to have
# anything installed on their machines except docker, so we just copy the
# raw files in and run npm install inside the docker container itself.
if [[ "$2" == "compose" ]]; then
  cp ../../dockerfiles/site.docker .
  cp -r ../../src/site/public .
  cp -r ../../src/site/src .
  cp ../../src/site/package.json .
else
  cd ../../src/site || exit 1
  npm run build || exit 1
  cd ../../build/site || exit 1
  cp -r ../../src/site/build/* .
fi
