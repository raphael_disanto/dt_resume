#!/usr/bin/env bash
# Script that kills any existing frontend server, dockerized or not.
# This script should only be called by
echo "Looking for PID for frontend server process"
if [ -f "site_pid.txt" ]; then
  pid=$(cat "site_pid.txt")
  kill -9 "$pid"
fi

