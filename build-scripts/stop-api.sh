#!/usr/bin/env bash
# Script that kills the locally running API server.

# Check for FastAPI
echo "Looking for pid for FastAPI server"
pid="$(pgrep -f server:app)"

if [[ -n "$pid" ]]; then
  echo "Killing process with PID $pid"
  pgid="$(ps --no-headers -p $pid -o pgid)"
  kill -SIGINT -- -"${pgid// /}"
fi

# Check for express
echo "Looking for pid for Express server"
pid="$(pgrep -f server.mjs)"

if [[ -n "$pid" ]]; then
  echo "Killing process with PID $pid"
  pgid="$(ps --no-headers -p $pid -o pgid)"
  kill -SIGINT -- -"${pgid// /}"
fi
