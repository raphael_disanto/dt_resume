#! /usr/bin/env bash
# Kills any existing DB docker, then spins up a new one.

working_dir=$(pwd)
this_dir="$working_dir/build-scripts"
"$this_dir"/stop-db.sh

cd ./build/db || exit 1
docker run -d --rm --name dt-resume-db -p 13533:5432 dt-resume-db
