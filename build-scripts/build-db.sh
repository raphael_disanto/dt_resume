#!/usr/bin/env bash
# Copies the appropriate files into the docker build folder, then builds
# the docker container

cd ./build || exit 1
rm -rf db
mkdir db
cd db || exit 1
cp -r ../../src/db/* .
cp "../../dockerfiles/db.docker" .

if [[ "$2" != "compose" ]]; then
  docker build -t dt-resume-db -f ./db.docker .
fi