#!/usr/bin/env bash
# This script builds the API. It's mostly included for parity with the other
# build/run processes, since python doesn't require a build step. However,
# We do move all code into the build folder (So they can be easily dockerized)
# so that gives us something to do in this script, at least
cd ./build || exit 1
rm -rf api
mkdir api
cd api || exit 1
api_folder="python"
if [[ "$1" == "express" ]]; then
  api_folder="express"
fi

cp -r ../../src/api/$api_folder/* .
cp ../../src/api/common/* .

if [[ "$2" == "compose" ]]; then
  cp ../../dockerfiles/api.docker .
fi
