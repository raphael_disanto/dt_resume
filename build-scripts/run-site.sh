#!/usr/bin/env bash
# Kills any existing frontend server, then spins up a new one.
#
# We can pass a parameter into this script to tell us if we're
# we're doing this via docker.

working_dir=$(pwd)
this_dir="$working_dir/build-scripts"
"$this_dir"/stop-site.sh

http-server -p 13531 -d false --proxy http://localhost:13531? ./build/site &
echo "$!" > site_pid.txt