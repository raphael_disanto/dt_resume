#!/usr/bin/env bash
# Kills any existing API server, then spins up a new one.
#
# We can pass a parameter into this script to tell us if we're
# doing this via docker.


working_dir=$(pwd)
this_dir="$working_dir/build-scripts"
"$this_dir"/stop-api.sh "$@"
export DB_HOST="localhost"
export DB_PORT="13533"
cd "$working_dir"/build/api || exit 1
./wait-for-postgres.sh "$@" &

