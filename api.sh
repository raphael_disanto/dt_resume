#!/usr/bin/env bash
# This script is just a wrapper around the build and run scripts for the API

./build-scripts/build-api.sh "$@"
./build-scripts/run-api.sh "$@"