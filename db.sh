#!/usr/bin/env bash
# This script is just a wrapper around the build and run scripts for the Database

./build-scripts/build-db.sh "$@"
./build-scripts/run-db.sh "$@"