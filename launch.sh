#!/usr/bin/env bash
# This script launches the entire 3-pillar system (Database, API, Frontend).
# It operates by calling scripts that manage the build/launch process for
# each of the systems.
# This script can be run in one of 4 ways:
#
# ./launch.sh - Shows the help message.
# ./launch.sh docker - Runs the entire stack under docker compose.
# ./launch.sh docker down - Stops the dockered stack.
# ./launch.sh dev - runs the site and the api locally (requires the nodejs
#   modules in ./src/site/package.json to be installed with npm install,
#   and the python modules in ./src/api/requirements.txt to be installed
#   with pip install.

set -e

chmod u+x ./*.sh
chmod u+x ./build-scripts/*.sh

# Clear the terminal and (attempt to clear the scrollback, too)
clear && echo -en "\033c"

# Does our build directory exist? If not, create it.
if [[ ! -d "./build" ]]; then
  echo "Build directory does not exist, creating it..."
  mkdir ./build
fi

if [[ "$#" -eq 0 ]]; then
  echo "Startup script. Run this using one of these commands:"
  echo "./launch.sh             - Shows this help message."
  echo "./launch.sh docker      - Runs the entire stack under docker compose."
  echo "./launch.sh docker down - Stops the docker compose stack."
  echo -n "./launch.sh dev         - Runs the site and the api locally \(requires "
  echo -n "the nodejs modules in ./src/site/package.json to be installed "
  echo -n "with npm install, and the python modules in "
  echo "./src/api/requirements.txt to be installed with pip install."
  echo "./launch.sh dev down    - Stops all locally-run pillars"
  echo "Additional parameters:"
  echo -n "  --site=[react|angular|vue] - Launches the version of the site with "
  echo "the requested framework. (Default: react)"
  echo -n "  --api=[fastapi|flask|express] - Launches the version of the API with "
  echo "the requested framework. (Default: fastapi)"
  echo -n "  --db=[postgres|mariadb|mongodb] - Launches the version of the DB with "
  echo "the requested database server. (Default: postgres)"
  exit 0
fi

site_type="react"
api_type="fastapi"
db_type="postgres"
run_type="dev"
up_down="up"

# Are we launching with different components than the defaults?
for param in "$@"; do
  case ${param} in
  docker)
    run_type="docker"
    ;;
  down)
    up_down="down"
    ;;
  --site=*)
    site_type="${param#*=}"
    ;;
  --api=*)
    api_type="${param#*=}"
    ;;
  --db=*)
    db_type="${param#*=}"
    ;;
  esac
done

if [[ "$site_type" != "react" ]]; then
  echo "Site type $site_type not yet supported!"
  exit 0
fi

if [[ "$api_type" != "fastapi" ]] && [[ "$api_type" != "express" ]]; then
  echo "API type $api_type not yet supported!"
  exit 0
fi

if [[ "$db_type" != "postgres" ]]; then
  echo "DB type $db_type not yet supported!"
  exit 0
fi

if [[ "$up_down" == "up" ]]; then
  echo "Launching $run_type stack with these component types:"
  echo "Site: $site_type"
  echo "API: $api_type"
  echo "Database: $db_type"
  read -rp "Is this correct? [y/n]: " continue
  if [[ "$continue" != "y" ]]; then
    echo "Exiting"
    exit 0
  fi
fi

if [[ "$up_down" == "down" ]]; then
  echo "Shutting docker stack down"
fi

# We should shut down any prior running components first
# Shut down any locally run pillars.
echo "Ensuring any locally-run pillars are shut down."
set +e
./build-scripts/stop-site.sh "$site_type"
./build-scripts/stop-api.sh "$api_type"
./build-scripts/stop-db.sh "$db_type"
set -e

# Now shut down the dockerized stack
# Copy the compose YAML file in
echo "Copying compose yaml file into build directory..."
cp ./dockerfiles/all.compose.yml ./build

cd ./build

echo "Shutting dockerized stack down..."
docker-compose -p dt-resume -f ./all.compose.yml down --remove-orphans

# If we've been told to shut everything down, we can exit here
if [[ "$up_down" == "down" ]]; then
  exit 0
fi

cd ..

if [[ "$run_type" == "dev" ]]; then
  echo -n "Launching stack in native mode. Please ensure all node modules "
  echo "and/or python modules are installed."

  # Database first (Because the API will want to connect to it)
  echo "Building and launching database..."
  ./db.sh "$db_type"

  # Now we can launch the API (Because the frontend will need to talk to it)
  echo "Building and launching backend API..."
  ./api.sh "$api_type"

  # And finally we can fire up the front end.
  echo "Building and launching frontend site..."
  ./site.sh "$site_type"
fi

if [[ "$run_type" == "docker" ]]; then
  echo "Launching stack under docker compose."
  echo "Copying database files to build directory for docker compose..."
  ./build-scripts/build-db.sh "$db_type" compose

  echo "Copying backend API files to build directory docker compose..."
  ./build-scripts/build-api.sh "$api_type" compose

  echo "Copying frontend site files to build directory docker compose..."
  ./build-scripts/build-site.sh "$site_type" compose

  # And use docker compose to actually stand everything up
  echo "Building all docker containers..."
  export BUILDKIT_PROGRESS=plain
  cd ./build
  docker-compose -p dt-resume -f all.compose.yml build
  # docker-compose -p dt-resume -f all.compose.yml up -d
fi
