#!/usr/bin/env bash

# Load data in this order, otherwise Postgres will be unhappy (Because we use foreign keys)
psql -U postgres dt_resume < ../seed-data/cleanup.sql
psql -U postgres dt_resume < ../seed-data/general_info.sql
psql -U postgres dt_resume < ../seed-data/education.sql
psql -U postgres dt_resume < ../seed-data/skill_categories.sql
psql -U postgres dt_resume < ../seed-data/skills.sql
psql -U postgres dt_resume < ../seed-data/work_experience.sql
psql -U postgres dt_resume < ../seed-data/work_achievements.sql
psql -U postgres dt_resume < ../seed-data/achievement_skills.sql

