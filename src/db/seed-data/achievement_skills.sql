create table achievement_skills
(
    obj_id         serial        primary key,
    skill_id       integer
        constraint achievement_skills_skills_obj_id_fk
            references skills,
    achievement_id integer
        constraint achievement_skills_work_achievements_obj_id_fk
            references work_achievements,
    user_id        integer
);

insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (1, 1, 1, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (2, 16, 1, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (3, 25, 1, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (4, 23, 1, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (6, 30, 1, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (7, 31, 1, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (8, 1, 2, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (9, 9, 2, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (10, 9, 1, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (11, 16, 2, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (12, 25, 2, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (13, 26, 2, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (14, 30, 2, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (15, 3, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (16, 1, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (17, 2, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (18, 5, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (19, 6, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (20, 10, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (21, 11, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (22, 14, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (23, 17, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (24, 30, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (25, 3, 4, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (26, 2, 4, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (27, 14, 4, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (28, 30, 4, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (29, 31, 4, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (30, 32, 4, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (31, 9, 4, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (33, 2, 5, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (34, 3, 5, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (36, 5, 5, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (37, 6, 5, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (38, 9, 5, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (39, 14, 5, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (40, 29, 5, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (42, 3, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (43, 2, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (44, 4, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (45, 9, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (46, 29, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (47, 30, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (48, 31, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (49, 27, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (50, 28, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (51, 3, 7, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (52, 14, 7, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (53, 29, 7, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (54, 3, 8, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (55, 14, 8, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (56, 29, 8, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (57, 28, 8, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (58, 8, 9, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (59, 5, 9, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (60, 6, 9, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (61, 3, 10, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (62, 2, 10, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (63, 7, 10, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (64, 14, 10, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (65, 29, 10, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (66, 3, 11, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (67, 7, 11, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (68, 9, 11, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (69, 14, 11, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (70, 3, 12, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (71, 7, 12, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (72, 27, 10, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (73, 28, 10, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (74, 33, 13, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (75, 14, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (76, 5, 14, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (77, 6, 14, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (78, 7, 14, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (79, 2, 14, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (80, 28, 14, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (81, 31, 3, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (82, 30, 7, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (83, 31, 7, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (84, 12, 6, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (86, 29, 9, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (87, 2, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (88, 9, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (89, 5, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (90, 6, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (91, 34, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (92, 16, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (93, 18, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (94, 17, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (95, 10, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (96, 11, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (97, 13, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (98, 35, 15, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (99, 1, 16, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (100, 36, 16, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (101, 37, 16, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (102, 38, 16, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (103, 39, 16, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (104, 38, 17, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (105, 37, 17, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (106, 1, 17, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (107, 37, 18, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (108, 39, 18, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (109, 1, 19, 1);
insert into achievement_skills (obj_id, skill_id, achievement_id, user_id)
values (110, 36, 19, 1);
