create table education
(
    obj_id         serial        primary key,
    school_name    text,
    course_studied text,
    date_from      timestamp with time zone,
    date_to        timestamp with time zone,
    user_id        integer
);

insert into education (obj_id, school_name, course_studied, date_from, date_to, user_id) values (2, 'Loughborough University of Technology', 'Systems Engineering', '1992-05-30 19:46:41.543000 +00:00', '1994-05-30 19:46:45.509000 +00:00', 1);
insert into education (obj_id, school_name, course_studied, date_from, date_to, user_id) values (1, 'Loughborough University of Technology', 'Ergonomics and Human Factors', '1994-05-30 19:46:26.436000 +00:00', '1996-05-10 19:46:33.667000 +00:00', 1);
