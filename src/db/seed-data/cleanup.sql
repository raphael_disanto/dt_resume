drop table if exists education;
drop table if exists achievement_skills;
drop table if exists work_achievements;
drop table if exists work_experience;
drop table if exists skills;
drop table if exists skill_categories;
drop table if exists general_info;
