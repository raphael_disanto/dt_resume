create table work_experience
(
    obj_id    serial        primary key,
    employer  text,
    date_from timestamp with time zone,
    date_to   timestamp with time zone,
    job_title text,
    user_id   integer
);

insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (5, 'Virgin Media', '1996-10-30 19:48:17.574000 +00:00', '1999-08-30 19:47:42.940000 +00:00', 'Senior Support Engineer', 1);
insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (1, 'Precisely (Previously known as Syncsort, acquired from Pitney Bowes, acquired from Maponics)', '2004-10-30 19:49:41.954000 +00:00', '2020-09-30 12:03:29.276000 +00:00', 'Senior Software Engineer', 1);
insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (2, 'Duar (Previously known as Acquiant)', '2001-08-30 19:49:05.995000 +00:00', '2003-05-30 19:49:23.343000 +00:00', 'Head of Software Development', 1);
insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (7, 'Respondent', '2020-11-01 00:00:00.239000 +00:00', '2021-11-01 17:33:26.369000 +00:00', 'Senior Software Engineer', 1);
insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (4, 'Treehouse Software/Savage Mojo', '1999-08-30 19:48:28.733000 +00:00', '2001-01-30 19:48:35.797000 +00:00', 'Software Developer', 1);
insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (6, '', '1992-11-05 10:40:06.926000 +00:00', '1996-10-05 10:40:13.396000 +00:00', 'Independent Contractor', 1);
insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (3, 'IS Innovative Software', '2001-01-30 19:48:45.138000 +00:00', '2001-05-30 19:48:52.010000 +00:00', 'Lead Programmer', 1);
insert into work_experience (obj_id, employer, date_from, date_to, job_title, user_id) values (8, 'Recharge', '2021-11-01 17:34:15.020000 +00:00', '2022-10-27 17:34:26.727000 +00:00', 'Senior Software Engineer', 1);
