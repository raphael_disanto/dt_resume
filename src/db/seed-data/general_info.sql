create table general_info
(
    first_name text,
    last_name  text,
    tagline    text,
    url        text,
    linkedin   text,
    summary    text,
    obj_id     serial
);

insert into general_info (first_name, last_name, tagline, url, linkedin, summary, obj_id) values ('Dave', 'Thomas', 'Making people''s lives easier with well-written software
', 'dt.cynage.com', 'linkedin.com/in/davethomas4/', 'Professional Computer Geek, full-stack Code Monkey, and Graphic Designer with 25+ years experience in the industry. Passionate about innovation, creativity, and problem solving. Worked in industries as diverse as gaming, financial, and GIS/Location Intelligence. Wholistic and systems-oriented, able to see the big picture while still focusing on the details. Requirements driven, customer-first attitude. The code should do exactly what the end user needs it to do.', 1);
