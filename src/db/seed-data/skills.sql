create table skills
(
    obj_id            serial primary key,
    skill_category_id integer
        constraint skills_skill_categories_obj_id_fk
            references skill_categories,
    skill_name        text,
    user_id           integer
);

insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (1, 1, 'Python', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (2, 1, 'JavaScript', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (3, 1, 'PHP', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (8, 1, 'TCL', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (9, 1, 'Shell Scripting', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (10, 2, 'Webpack', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (11, 2, 'Babel', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (12, 2, 'Flask', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (13, 2, 'Node', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (14, 2, 'Apache', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (15, 2, 'ExpressJS', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (16, 2, 'Docker', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (18, 2, 'Vue', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (19, 2, 'React', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (20, 3, 'Linux', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (21, 3, 'OSX', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (22, 3, 'Windows', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (23, 3, 'AWS', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (24, 3, 'Serverless', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (25, 4, 'Jenkins', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (26, 4, 'Github/Gitlab', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (27, 5, 'UI/UX', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (28, 5, 'Photoshop', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (29, 3, 'MySQL/MariaDB', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (30, 3, 'Postgres', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (31, 3, 'PostGIS', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (32, 5, 'Drupal', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (33, 1, 'VisualBasic', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (17, 2, 'AngularJS', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (5, 1, 'HTML', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (6, 1, 'CSS', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (4, 1, 'C#', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (7, 1, 'PERL', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (34, 1, 'TypeScript', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (35, 3, 'MongoDB', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (38, 3, 'Splunk', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (37, 3, 'Terraform', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (36, 3, 'GCP', 1);
insert into skills (obj_id, skill_category_id, skill_name, user_id)
values (39, 3, 'SignalFX', 1);
