create table work_achievements
(
    obj_id      serial        primary key,
    employer_id integer
        constraint work_achievements_work_experience_obj_id_fk
            references work_experience,
    description text,
    user_id     integer
);

insert into work_achievements (obj_id, employer_id, description, user_id) values (1, 1, 'Wrote many in-house tools that replaced a significant portion of manual data processing tasks with automation through the entire pipeline; from ETL to data processing to final data extraction and packaging.
', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (2, 1, 'Wrote an application framework that automated most of the boilerplate code and configuration required in building an app for a CI/CD deployment pipeline.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (3, 1, 'Created web-based data filtering tools that utilized a drag-and-drop web UI, translating user actions into composite SQL queries that allowed non-technical users to join and filter complex datasets.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (4, 1, 'Developed, managed and supported (sub-24hr turnaround) a mission-critical customer-facing API that allowed customers a la carte access to our spatial datasets, handling around 5 million requests per month.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (5, 2, 'Wrote a fully UI-driven financial website generator, that allowed plug-and-play for drop in components based on the user''s requirements', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (6, 1, 'Wrote a multi-language, multi-platform product management, order-fulfilment tool, with a combined Windows Desktop and Web UI that automated order production and delivery.
', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (7, 1, 'One of three engineers that moved Maponics out of physical mapping and into location intelligence by writing automated tools and processes that collected, extracted and delivered data.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (8, 1, 'Designed, built, and deployed a custom ecommerce solution to fit a highly configurable and customizable set of mapping products and spatial data.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (9, 3, 'TCL developer on financial markets informatics systems', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (10, 4, 'Flash, JavaScript, and back end PHP developer, working on a web-based MMO.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (11, 5, 'Developed NTL Internet''s first support Knowledge Base', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (12, 5, 'Wrote internal software support tools for making Front Line Support Engineers'' lives easier', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (13, 5, 'Front line support, eventually promoted to second-line engineer/back office developer.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (14, 6, 'Developed various websites on contract during the very early days of the web.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (15, 7, 'Part of a team that developed and maintained multiple dockerized backend REST APIs in JavaScript/TypeScript that connected to MongoDB, Postgres, and Redis, driving a number of high-traffic web applications and serverless lambdas. Also developed and maintained the web applications themselves.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (16, 8, 'Supported and developed new features for a busy (peaks of millions of events per hour) distributed platform e-commerce support stack. ', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (17, 8, 'Designed and Implemented a replacement GCP->Splunk logging solution that allowed Platform Services to log 10x more events per minute without delays.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (18, 8, 'Part of a pair of engineers that migrated our full suite of Platform Services SignalFX detectors and alerts into Terraform.', 1);
insert into work_achievements (obj_id, employer_id, description, user_id) values (19, 8, 'Part of a team that added an entirely new feature to Recharge''s back end Platform that reduced customer search times dramatically, improving user experience.', 1);
