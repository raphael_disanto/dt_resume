create table skill_categories
(
    obj_id              serial primary key,
    skill_category_name text,
    user_id             integer
);

insert into skill_categories (obj_id, skill_category_name, user_id)
values (1, 'Languages', 1);
insert into skill_categories (obj_id, skill_category_name, user_id)
values (2, 'Stack Technologies', 1);
insert into skill_categories (obj_id, skill_category_name, user_id)
values (4, 'Pipeline Tools', 1);
insert into skill_categories (obj_id, skill_category_name, user_id)
values (5, 'Other Skills', 1);
insert into skill_categories (obj_id, skill_category_name, user_id)
values (3, 'Architecture/Systems', 1);
