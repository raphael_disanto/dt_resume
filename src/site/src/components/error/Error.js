import styled from "styled-components"
import {useNavigate} from "react-router-dom"

export const ErrorPage = () => {

  const navigate = useNavigate()

  const ErrorDiv = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;
  `
  const ErrorWrapper = styled.div`
    width: 50%;
    margin: auto;
    text-align: center;
    border: 1px solid var(--inverse-text-color);
  `
  return (
    <ErrorDiv>
      <ErrorWrapper>
        <h1>Uhoh!</h1>
        <p>
          Something went wrong.
        </p>
        <p>
          That probably means Dave messed up somewhere. You probably won't be
          able to do anything to fix it. You'll just have to let him know
          what you were doing when this error happened so he can sledgehammer
          a a hastily put-together and inherently unstable patch into the code!
        </p>
        <p>
          In the meantime, you could <span className="clickable">go back</span> to
          where you were and try exploring somewhere else on the site.
        </p>
      </ErrorWrapper>
    </ErrorDiv>
  )
}