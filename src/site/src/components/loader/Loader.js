import {useEffect} from "react"
import {useDispatch} from "react-redux"

import {Main} from "../main/Main"

import {storeUser} from "../../store/slices/user-slice"
import {storeCategories, storeSkills} from "../../store/slices/skills-slice"
import {storeEducation} from "../../store/slices/education-slice"
import {storeWorkExperience, storeWorkAchievements} from "../../store/slices/work-experience-slice"
import {GetLoadingData, storeCurrentPage, storeLoadingData} from "../../store/slices/settings-slice"


/**
 * Loader div. This wraps all other content, ensuring that our data gets loaded
 * before we do anything.
 * @param pageName
 * @returns {JSX.Element}
 * @constructor
 */
export const Loader = ({pageName}) => {
  const loadingData = GetLoadingData()
  const dispatch = useDispatch()

  dispatch(storeCurrentPage(pageName))

  /**
   * Worker function that loads everything and writes it to the store
   * @returns {Promise<boolean>}
   */
  const getInitialData = async () => {
    // Hardcoded in this app, but could be eventually parameterized.
    const userId = 1

    // Load all the data from the API up front.
    const thisHost = window.location.hostname
    const userResponse = await fetch("http://" + thisHost + ":13532/user/" + userId)
    const skillsResponse = await fetch("http://" + thisHost + ":13532/skills/" + userId)
    const skillCategoriesResponse = await fetch("http://" + thisHost + ":13532/skillCategories/" + userId)
    const workExperienceResponse = await fetch("http://" + thisHost + ":13532/employment/" + userId)
    const educationResponse = await fetch("http://" + thisHost + ":13532/education/" + userId)
    const workAchievementResponse = await fetch("http://" + thisHost + ":13532/achievements/" + userId)

    // Grab all the actual data from the response JSON. TThis data
    // comes into us in the output format set in the API.
    // (https://gitlab.com/raphael_disanto/dt_resume/-/blob/main/src/api/helpers/lib.py#L6)
    const userData = await userResponse.json()
    const skillsData = await skillsResponse.json()
    const skillCategoriesData = await skillCategoriesResponse.json()
    const workExperienceData = await workExperienceResponse.json()
    const workAchievementData = await workAchievementResponse.json()
    const educationData = await educationResponse.json()

    if (
      userData.success &&
      skillsData.success &&
      skillCategoriesData.success &&
      workExperienceData.success &&
      educationData.success
    ) {
      dispatch(storeUser(userData.data[0]))
      dispatch(storeSkills(skillsData.data))
      dispatch(storeCategories(skillCategoriesData.data))
      dispatch(storeEducation(educationData.data))
      dispatch(storeWorkExperience(workExperienceData.data))
      dispatch(storeWorkAchievements(workAchievementData.data))
      dispatch(storeLoadingData(false))
    }
  }

  // Run once on startup.
  useEffect(() => {
    // We can ignore the returned promise because we don't actually return
    // anything from getInitialData
    // noinspection JSIgnoredPromiseFromCall
    getInitialData()
  }, [])

  // Now return the template using Main if the loader has actually finished
  return (
    <div className="App">
      {
        loadingData
          ? <div>Loading...</div>
          : <Main/>
      }
    </div>
  )
}