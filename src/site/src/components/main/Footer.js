import styled from "styled-components"

import {format} from "date-fns"

import {GetUser} from "../../store/slices/user-slice"

/**
 * Displays the Footer at the bottom of the page
 * @returns {JSX.Element}
 * @constructor
 */
export const FooterWrapper = () => {
  /** @type {User} **/
  const user = GetUser()
  const year = format(new Date(), "yyyy")
  const Footer = styled.div`
    background: black;
    text-align: center;
    padding: 5px;
    font-size: xx-small;
    position: fixed;
    z-index: 300;
    bottom: 0;
    width: 100%;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: var(--inverse-text-color);
  `
  return (
    <Footer id="footer">
      <div>
        &copy; {year} Dave Thomas
      </div>
      <div>
        <a target="_new" className="clickable inverse" href={"https://" + user.linkedin}>{user.linkedin}</a>
      </div>
      <div>
        Built with FastAPI, React, and Postgres.
        Code available on <a target="_new" className="clickable inverse" href="https://gitlab.com/raphael_disanto/dt_resume">gitlab</a>.
      </div>
    </Footer>
  )
}