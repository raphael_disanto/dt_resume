import styled from "styled-components"

import {ContentWrapper} from "../content-wrapper/ContentWrapper"
import {MenuComponent} from "./Menu"
import {FooterWrapper} from "./Footer"
import {HeaderWrapper} from "./Header"

import {GetCurrentPage} from "../../store/slices/settings-slice"

/**
 * Main div. Basically the wrapper for the entire side (although it lives within the other
 * non-display divs required by React, like the router provider and redux provider.
 * @returns {JSX.Element}
 * @constructor
 */
export const Main = () => {
  const currentPage = GetCurrentPage()
  const {backgroundColor, textColor} = currentPage === "Home" || currentPage === ""
    ? {backgroundColor: "black", textColor: "var(--inverse-text-color)"}
    : {backgroundColor: "white", textColor: "var(--text-color)"}


  const SiteWrapper = styled.div`
    height: 100%;
    width: 100%;

    input[type=checkbox]#main-menu-hamburger-dropdown-controller {
      /* display: none; */
    }

    input[type=checkbox]#main-menu-hamburger-dropdown-controller + div {
      transition: 0.5s;
      top: -5em;
    }

    input[type=checkbox]#main-menu-hamburger-dropdown-controller:checked + div {
      transition: 0.5s;
      top: 6em;
    }
  `
  const SiteContent = styled.div`
    position: fixed;
    display: block;
    top: 100px;
    left: 0;
    height: calc(100% - 150px);
    overflow-y: auto;
    color: ${textColor};
    width: 100%;
  `

  const VerticalMenu = styled.div`
    position: fixed;
    right: 0;
    display: block;
    z-index: 10;
    background-color: black;
    padding: 10px;

    @media screen and (min-width: 48rem) {
      display: none;
    }
  `

  return (
    <SiteWrapper id="site-wrapper">
      {/* We put the vertical menu here, so it can be behind the main header section */}
      {/* Using the CSS checkbox hack to hide/show the vertical menu */}
      <input type="checkbox" id="main-menu-hamburger-dropdown-controller"/>
      <VerticalMenu id="vertical-menu">
        <MenuComponent direction="vertical"/>
      </VerticalMenu>
      <HeaderWrapper id="header-wrapper"/>
      <SiteContent id="site-content">
        <ContentWrapper/>
      </SiteContent>
      <FooterWrapper/>
    </SiteWrapper>
  )
}