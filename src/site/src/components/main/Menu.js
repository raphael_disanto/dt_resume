import styled from "styled-components"
import {Link} from "react-router-dom"

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faKeyboard} from "@fortawesome/free-solid-svg-icons"
import {faBriefcase} from "@fortawesome/free-solid-svg-icons"
import {faSchool} from "@fortawesome/free-solid-svg-icons"
import {faHome} from "@fortawesome/free-solid-svg-icons"

/**
 * Renders a menu (either horizontally or vertically)
 *
 * @param direction
 * @returns {JSX.Element}
 * @constructor
 */
export const MenuComponent = ({direction}) => {

  const menu = {
    "Home": faHome,
    "Skills": faKeyboard,
    "Work": faBriefcase,
    "School": faSchool,
  }
  const HamburgerMenuListItem = styled.li`
    margin-bottom: 10px;
  `

  const HorizontalMenu = styled.ul`
    padding-top: 10px;
    li:not(:last-child) {
      padding-right: 10px;
    }
  `

  if (direction=== "horizontal") {
    return (
      <HorizontalMenu id="main-menu" className="bare-list horizontal-list">
        {
          Object.entries(menu).map(([displayText, icon]) => {
            return <li className="centered">
              <Link className="clickable inverse" to={"/" + displayText.toLowerCase()}>
                {displayText}
                <br/>
                <FontAwesomeIcon icon={icon}/>
              </Link>
            </li>
          })
        }
      </HorizontalMenu>
    )
  } else {
    return (
      <ul id="main-menu-hamburger" className="bare-list">
          {
            Object.entries(menu).map(([displayText, icon]) => {
              return <HamburgerMenuListItem className="clickable">
                <Link className="clickable inverse" to={"/" + displayText.toLowerCase()}>
                  <FontAwesomeIcon icon={icon}/>
                  {displayText}
                </Link>
              </HamburgerMenuListItem>
            })
          }
        </ul>
    )
  }
}