import styled from "styled-components"
import {Link} from "react-router-dom"

import {MenuWrapper} from "./MenuWrapper"

import {GetUser} from "../../store/slices/user-slice"

import siteLogo from "./site-icon.png"

export const HeaderWrapper = () => {
  /** @type {User} **/
  const user = GetUser()
  const HeaderWrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    height: 100px;
    z-index: 200;
    background-color: black;
    color: var(--inverse-text-color);
    width: 100%;
    padding: 10px;
    display: flex;
    flex-direction: row;
  `

  const NameWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;

    #site-logo {
      margin-right: 10px;
    }

    #resume-name-full {
      display: none;
      font-size: xxx-large;
      font-weight: bold;
    }

    #resume-name-small {
      display: inline-block;
      font-weight: bold;
      font-size: large;
    }

    @media screen and (min-width: 48rem) {
      #resume-name-full {
        display: inline-block;
      }

      #resume-name-small {
        display: none;
      }
    }
  `

  const Tagline = styled.div`
    font-style: italic;
    font-size: medium;
    white-space: normal;
  `

  const HeaderLeftSection = styled.div`
    flex-grow: 2;
  `
  return (
    <HeaderWrapper id="header-wrapper">
      <HeaderLeftSection id="header-left-section">
        <NameWrapper id="name-wrapper">
          <Link to={"/"}>
            <img className="clickable" id="site-logo" src={siteLogo} alt="Site Logo" height="40" width="40"/>
          </Link>
          <span id="resume-name-full" className="name nowrap">{user.firstName} {user.lastName}</span>
          <span id="resume-name-small" className="nowrap">{user.firstName}<br/>{user.lastName}</span>
        </NameWrapper>
        <Tagline id="tagline" className="nowrap">
          {user.tagline}
        </Tagline>
      </HeaderLeftSection>
      <MenuWrapper/>
    </HeaderWrapper>
  )
}