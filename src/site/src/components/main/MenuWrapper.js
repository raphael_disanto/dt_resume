import styled from "styled-components"

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faBars} from "@fortawesome/free-solid-svg-icons"

import {MenuComponent} from "./Menu"

export const MenuWrapper = () => {
  const InnerMenuWrapper = styled.div`
    padding-top: 10px;
    padding-right: 10px;
    text-align: right;
    z-index: 100;
  `
  const HorizontalMenu = styled.div`
    display: none;

    @media screen and (min-width: 48rem) {
      display: block;
    }
  `

  const HamburgerMenuIcon = styled.label`
    display: block;
    font-size: xxx-large;
    background-color: black;

    @media screen and (min-width: 48rem) {
      display: none;
    }
  `
  return (
    <InnerMenuWrapper id="inner-menu-wrapper">
        <HorizontalMenu id="horizontal-menu">
          <MenuComponent direction="horizontal"/>
        </HorizontalMenu>
        <HamburgerMenuIcon htmlFor="main-menu-hamburger-dropdown-controller">
          <FontAwesomeIcon className="clickable inverse" icon={faBars}/>
        </HamburgerMenuIcon>
    </InnerMenuWrapper>
  )
}