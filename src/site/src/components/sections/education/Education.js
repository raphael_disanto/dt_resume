import styled from "styled-components"
import {useSelector} from "react-redux"

import {School} from "./School";

import {sortArrayByObjectField} from "../../../lib"

import educationBackground from "./education-background.png"

/**
 * This is the main Education page. It simply provides a wrapper for the
 * individual School components.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export const EducationDisplay = () => {
  const allEducationData = useSelector((state) => state.education)
  const internalEducationData = [...allEducationData]
  sortArrayByObjectField(internalEducationData, "dateFrom", "desc")

  const EducationWrapper = styled.div`
    @media screen and (min-width: 48rem) {
      background-image: url(${educationBackground});
      background-position: bottom right;
      background-repeat: no-repeat;
      background-size: 400px;
    }
  `

  return (
    <EducationWrapper id="education-wrapper" className="main-content">
        {
          internalEducationData.map((s) => <School schoolData={s}/>)
        }
    </EducationWrapper>
  )
}