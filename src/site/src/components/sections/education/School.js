import styled from "styled-components"

import {convertDate} from "../../../lib"

/**
 * Displays a single School item.
 *
 * @param data
 * @returns {JSX.Element}
 * @constructor
 */
export const School = (data) => {
  const schoolData = data["schoolData"]

  const dateFrom = convertDate(schoolData.dateFrom)
  const dateTo = convertDate(schoolData.dateTo)

  const SchoolDiv = styled.div`
    margin-bottom: 40px;
  `
  return (
    <SchoolDiv id={"school-" + schoolData["objId"]}>
      <h3>
        {schoolData.schoolName}
      ({dateFrom} - {dateTo})
        </h3>
      {schoolData.courseStudied}
    </SchoolDiv>
  )
}
