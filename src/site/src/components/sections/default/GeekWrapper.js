import styled from "styled-components"
import {Link} from "react-router-dom"
import portrait from "./DT-2023-01-29.png"

const GeekWrapperDiv = styled.div`
  position: relative;
  display: flex;
  padding: 0;

  #portrait {
    display: none;
  }

  @media screen and (min-width: 48rem) {
    #portrait {
      display: block;
    }
  }
`

const GeekTextDiv = styled.div`
  padding: 30px;

  h4 {
    margin-bottom: 0;
  }

  h4 + span {
    font-style: italic;
    font-size: small;
  }
`

export const GeekWrapper = () => {
  return (
    <GeekWrapperDiv className="homepage-section-text main-content-card">
      <div id="portrait">
        <img src={portrait} alt="DT Portrait" height="600"/>
      </div>
      <GeekTextDiv>
        <h4>Spiro Ergo Creo</h4>
        <span>I breathe, therefore I create.</span>
        <p>Been in WebDev since 1992.</p>
        <p>
          Problem solver. Systems orientated. Big picture, small picture; it's all part of
          the same picture.
        </p>
        <p>
          From game development to Location Intelligence, FinTech to digital marketplaces.
          The field doesn't matter, but a socially conscious mission is a big plus.
          Software should make people's lives better.
        </p>
        <p>
          Computers should do what computers are good at, so that people can do what
          people are good at.
        </p>
        <div className="centered">
          <Link className="clickable" to="/work">
            See where I've worked and what I've worked on
          </Link>
        </div>
      </GeekTextDiv>
    </GeekWrapperDiv>
  )
}