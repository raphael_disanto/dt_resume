import styled from "styled-components"
import {Link} from "react-router-dom"

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faServer} from "@fortawesome/free-solid-svg-icons"
import {faCode} from "@fortawesome/free-solid-svg-icons"
import {faCogs} from "@fortawesome/free-solid-svg-icons"
import {faTerminal} from "@fortawesome/free-solid-svg-icons"

const MonkeyWrapperDiv = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  text-align: center;

  ul {
    padding-bottom: 20px;
  }
  
  svg {
    font-size: larger;
    font-weight: bold;
  }
`

export const MonkeyWrapper = () => {
  return (
    <div className="homepage-section-text main-content-card">
      <MonkeyWrapperDiv>
        <div>
          <FontAwesomeIcon icon={faCode}/>
          <h3>Frontend</h3>
          <ul className="bare-list">
            <li>Vue</li>
            <li>React</li>
            <li>Photoshop</li>
            <li>Javascript</li>
          </ul>
        </div>
        <div>
          <FontAwesomeIcon icon={faTerminal}/>
          <h3>Backend</h3>
          <ul className="bare-list">
            <li>Python</li>
            <li>Node</li>
            <li>Postgres</li>
            <li>MongoDB</li>
          </ul>
        </div>
        <div>
          <FontAwesomeIcon icon={faServer}/>
          <h3>Server</h3>
          <ul className="bare-list">
            <li>Linux</li>
            <li>MacOS</li>
            <li>Windows</li>
            <li>Apache</li>
          </ul>
        </div>
        <div>
          <FontAwesomeIcon icon={faCogs}/>
          <h3>Infra</h3>
          <ul className="bare-list">
            <li>Git</li>
            <li>Docker</li>
            <li>Terraform</li>
            <li>AWS & GCP</li>
          </ul>
        </div>
      </MonkeyWrapperDiv>
      <div className="centered">
        <Link className="clickable" to="/skills">
          View the Full List
        </Link>
      </div>
    </div>
  )
}