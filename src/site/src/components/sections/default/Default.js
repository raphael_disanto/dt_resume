import {useEffect} from "react"
import styled from "styled-components"

import {GetUser} from "../../../store/slices/user-slice"

import FirefallImage from "./Firefall.png"
import BashImage from "./Bash.png"
import JavaScriptImage from "./JavaScript.png"
import MusicImage from "./Music.png"
import PythonImage from "./Python.png"

import {MonkeyWrapper} from "./MonkeyWrapper"
import {GeekWrapper} from "./GeekWrapper"

const SummaryDiv = styled.div`
  font-style: italic;
  text-align: center;
  display: flex;
  justify-content: center;
  align-content: center;
  margin: auto;
  color: var(--inverse-text-color);
  position: relative;

  @media screen and (min-width: 48rem) {
    height: calc(100VH - 160px);
  }
`

const SummaryContent = styled.div`
  background-color: rgb(0, 0, 0, 70%);
  border: double var(--inverse-text-color);
  padding: 20px;
  width: 75%;
  margin: auto;
  color: var(--inverse-text-color);
  font-size: medium;


  @media screen and (min-width: 48rem) {
    font-size: xx-large;
  }
`

const HomepageWrapper = styled.div`
  position: relative;
  display: flex;
  min-height: calc(100VH - 160px);
  flex-direction: column;
  overflow: hidden;
  padding-bottom: 50px;
`

const SummaryBackgroundDiv = styled.div`
  background: linear-gradient(black, transparent 95%);
  position: absolute;
  z-index: -3;
  height: 100%;
  width: 100%;
`

const SummaryForegroundDiv = styled.div`
  background: linear-gradient(black, transparent 25%);
  position: absolute;
  z-index: 1;
  height: 100%;
  width: 100%;
`

const FirefallDiv = styled.div`
  position: absolute;
  z-index: -2;
  max-height: 100vh;
  max-width: 100vh;
  animation: fadeIn 0.5s ease-out;
  display: flex;
  justify-content: center;
  opacity: 50%;
`

const BashDiv = styled.div`
  position: absolute;
  z-index: -1;
  top: 0;
  right: 0;
  animation: slideLeft 0.5s ease-out;
`
const PythonDiv = styled.div`
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  animation: slideDown 0.5s ease-out;
`
const MusicDiv = styled.div`
  position: absolute;
  z-index: -1;
  bottom: 0;
  right: 0;
  animation: slideUp 0.5s ease-out;
`
const JavaScriptDiv = styled.div`
  position: absolute;
  z-index: -1;
  bottom: 0;
  left: 0;
  animation: slideRight 0.5s ease-out;
`


/**
 * Default page. This is the homepage, or "splash" page. This gets rendered for
 * the paths "/" or "/home"
 *
 * @returns {JSX.Element}
 * @constructor
 */
export const DefaultDisplay = () => {

  let geekDiv
  let monkeyDiv
  let graphicsDiv
  let wrapperHeight
  const elementVisibleBuffer = 100
  const revealDiv = () => {
    const geekDivTop = geekDiv.getBoundingClientRect().top
    const monkeyDivTop = monkeyDiv.getBoundingClientRect().top
    const graphicsDivTop = graphicsDiv.getBoundingClientRect().top

    if (geekDivTop < wrapperHeight - elementVisibleBuffer) {
      geekDiv.classList.add("active")
    }
    if (monkeyDivTop < wrapperHeight - elementVisibleBuffer) {
      monkeyDiv.classList.add("active")
    }
    if (graphicsDivTop < wrapperHeight - elementVisibleBuffer) {
      graphicsDiv.classList.add("active")
    }
  }
  useEffect(() => {
    geekDiv = document.getElementById("computer-geek-div")
    monkeyDiv = document.getElementById("code-monkey-div")
    graphicsDiv = document.getElementById("graphic-designer-div")
    const wrapperDiv = document.getElementById("site-content")
    wrapperDiv.addEventListener("scroll", revealDiv)
    wrapperHeight = window.innerHeight

    return () => {
      wrapperDiv.removeEventListener("scroll", revealDiv)
    }
  }, [])

  /** @type {User} **/
  const user = GetUser()
  return (
    <HomepageWrapper id="homepage-wrapper">
      <SummaryDiv id="summary-div">
        <SummaryBackgroundDiv id="summary-background"></SummaryBackgroundDiv>
        <SummaryForegroundDiv id="summary-foreground"></SummaryForegroundDiv>
        <FirefallDiv id="firefall">
          <img className="firefall-background-image" alt="Firefall Background Image" src={FirefallImage}/>
        </FirefallDiv>
        <BashDiv id="bash">
          <img className="bash-background-image" alt="Shell Scripting Background Image" src={BashImage}/>
        </BashDiv>
        <PythonDiv id="python">
          <img className="python-background-image" alt="Python Background Image" src={PythonImage}/>
        </PythonDiv>
        <MusicDiv id="music">
          <img className="music-background-image" alt="Music Background Image" src={MusicImage}/>
        </MusicDiv>
        <JavaScriptDiv id="javascript">
          <img className="js-background-image" alt="JavaScript Background Image" src={JavaScriptImage}/>
        </JavaScriptDiv>
        <SummaryContent id="summary-content">
          {user.summary}
        </SummaryContent>
      </SummaryDiv>
      <div id="computer-geek-div" className="homepage-section reveal">
        <h1>Professional Computer Geek</h1>
        <GeekWrapper />
      </div>
      <div id="code-monkey-div" className="homepage-section reveal">
        <h1>Full Stack Code Monkey</h1>
        <MonkeyWrapper />
      </div>
      <div id="graphic-designer-div" className="homepage-section reveal">
        <h1>Graphic Designer</h1>
        <div className="homepage-section-text main-content-card">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
          exercitation ullamco laboris nisi ut aliquip ex ea
        </div>
      </div>
    </HomepageWrapper>
  )
}