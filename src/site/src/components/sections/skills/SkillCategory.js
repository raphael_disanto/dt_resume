import styled from "styled-components"

import "./skills.css"
import {GetSkillCategoryById, GetSkillsByCategory} from "../../../store/slices/skills-slice"

/**
 * Renders an individual Skill Category card, including all the skills
 * within that category.
 *
 * @param data
 * @returns {JSX.Element}
 * @constructor
 */
export const SkillCategoryDiv = (data) => {
  const thisSkillCategoryData = GetSkillCategoryById(data.skillCategoryId)
  const thisSkillCategorySkills = GetSkillsByCategory(data.skillCategoryId)
  const SkillCategoryDiv = styled.div`
    flex: 1;
    margin-left: 10px;
    margin-right: 10px;
    margin-bottom: 10px;
    padding-bottom: 10px;
    padding-left: 5px;
    padding-right: 5px;
  `
  const SkillCategoryName = styled.h3`
    padding-bottom: 10px;
  `
  return (
    <SkillCategoryDiv id={"skill-category-" + thisSkillCategoryData.objId} className="nowrap main-content-card">
      <SkillCategoryName className="centered">{thisSkillCategoryData.skillCategoryName}</SkillCategoryName>
      <ul className="bare-list">
        {
          thisSkillCategorySkills.map(skillData => {
            return (
              <li className="centered">{skillData.skillName}</li>
            )
          })
        }
      </ul>
    </SkillCategoryDiv>
  )
}