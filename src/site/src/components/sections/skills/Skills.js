import styled from "styled-components"

import {SkillCategoryDiv} from "./SkillCategory"
import {GetAllSkillCategories} from "../../../store/slices/skills-slice"

import skillsBackground from './skills-background.png'

/**
 * This is the main Skills page. It simply serves as a wrapper for each individual
 * Skill Category component.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export const SkillsDisplay = () => {
  const allSkillCategoryData = GetAllSkillCategories()
  const SkillsContent = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: wrap;
  `
  const SkillsWrapper = styled.div`
    
    @media screen and (min-width: 48rem) {
      background-image: url(${skillsBackground});
      background-position: bottom right;
      background-repeat: no-repeat;
      background-size: 400px;
    }
  `

  return (
    <SkillsWrapper id="skills-wrapper" className="main-content">
      <SkillsContent id="skills-content">
        {
          allSkillCategoryData.map(skillCategoryData => {
            return (
              <SkillCategoryDiv skillCategoryId={skillCategoryData.objId}
              />
            )
          })
        }
      </SkillsContent>
    </SkillsWrapper>
  )
}