import styled from "styled-components"

import {GetAchievementById} from "../../../store/slices/work-experience-slice"
import {GetSkillById} from "../../../store/slices/skills-slice"

/**
 * Renders a single Work Achievement inside a Work Experience section,
 * including that Achievement's used Skills.
 * @param data
 * @returns {JSX.Element}
 * @constructor
 */
export const WorkAchievementDiv = (data) => {
  const achievementId = data["workAchievementId"]
  const achievementData = GetAchievementById(achievementId)
  const achievementSkills = achievementData.skills
    .map(skillId => {
      const thisSKill = GetSkillById(skillId)
      return thisSKill.skillName
    }).join(", ")

  const WorkAchievementDescriptionWrapper = styled.div`
    padding-top: 10px;
  `

  const WorkAchievementSkills = styled.div`
    font-size: x-small;
  `
  return (
    <WorkAchievementDescriptionWrapper>
      <div>{achievementData.description}</div>
      <WorkAchievementSkills>
        Skills Used: {achievementSkills}
      </WorkAchievementSkills>
    </WorkAchievementDescriptionWrapper>
  )
}