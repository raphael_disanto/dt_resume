import styled from "styled-components"

import {convertDate} from "../../../lib"
import {WorkAchievementDiv} from "./WorkAchievement"
import {GetAchievementByExperienceId, GetWorkExperienceById} from "../../../store/slices/work-experience-slice"

/**
 * Displays a single Work Experience, including that experience's achievements.
 * @param data
 * @returns {JSX.Element}
 * @constructor
 */
export const WorkExperienceDiv = (data) => {
  const workExperienceId = data["workExperienceId"]
  const workExperienceData = GetWorkExperienceById(workExperienceId)

  // Grab all the achievements associated with this work experience. This list is
  // automatically filtered by selected skills (if any), so we don't have to do
  // that again.
  const workExperienceAchievements = GetAchievementByExperienceId(workExperienceId)
  const employerName = workExperienceData.employer
    ? workExperienceData.employer + ": "
    : ""

  const dateFrom = convertDate(workExperienceData.dateFrom, true)
  const dateTo = convertDate(workExperienceData.dateTo, true)
  const WorkExperienceWrapper = styled.div`
    padding: 10px;
    margin-bottom: 20px;
  `

  return (
    <WorkExperienceWrapper id={"work-experience-" + workExperienceData.objId}
                           className="single-work-experience-wrapper main-content-card">
      <h3>{employerName} {dateFrom} - {dateTo}</h3>
      <div>{workExperienceData.jobTitle}</div>
      {
        workExperienceAchievements.map(workAchievement => {
          return (
            <WorkAchievementDiv workAchievementId={workAchievement.objId}/>
          )
        })
      }
    </WorkExperienceWrapper>
  )
}