import styled from "styled-components"
import {useDispatch} from "react-redux"

import Multiselect from "react-widgets/Multiselect"

import {GetAllSkills} from "../../../store/slices/skills-slice"
import {GetFilterTerms, storeFilterTerms} from "../../../store/slices/settings-slice"
import {sortArrayByObjectField} from "../../../lib"

import "react-widgets/styles.css"

/**
 * Renders the skill filter on the Employment page. This uses the multiselect
 * widget from react-widgets to give us the tag-based select functionality.
 * @returns {JSX.Element}
 * @constructor
 */
export const EmploymentFilter = () => {
  const dispatch = useDispatch()
  const currentFilterTerm = GetFilterTerms()
  const EmploymentFilterWrapper = styled.div`
    display: flex;
    flex-direction: row;
    padding-bottom: 10px;
    align-items: center;
  `
  const EmploymentFilterInputWrapper = styled.div`
    padding-left: 10px;
    padding-right: 5px;
    flex-grow: 1;
  `

  const allSkills = GetAllSkills()

  // react-widget's dropdown will accept a list of key/value objects, so we'll
  // convert the list of skills into that form. We have to use the skill IDs as keys,
  // because that's what we'll be filtering on.
  const skillsForDropDown = allSkills.map(skill => {return {id: skill.objId, name: skill.skillName}})
  sortArrayByObjectField(skillsForDropDown, "name")

  // Keep the filterTerms field in the store updated as the user adds and removes skills.
  const setFilterTerms = (filterTerms) => {
    dispatch(storeFilterTerms(filterTerms.map(filter => filter.id)))
  }

  // noinspection JSValidateTypes
  return (
    <EmploymentFilterWrapper>
      <div>
        Filter Projects By Skill:
      </div>
      <EmploymentFilterInputWrapper>
        <Multiselect
          value={currentFilterTerm}
          onChange={setFilterTerms}
          defaultValue={[]}
          data={skillsForDropDown}
          dataKey="id"
          textField="name"
        />
      </EmploymentFilterInputWrapper>
    </EmploymentFilterWrapper>
  )
}