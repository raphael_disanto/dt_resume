import styled from "styled-components"

import {WorkExperienceDiv} from "./WorkExperience"
import {EmploymentFilter} from "./employment-filter"
import {sortArrayByObjectField} from "../../../lib"
import {GetAllWorkExperiences} from "../../../store/slices/work-experience-slice"

import employmentBackground from "./employment-background.png"

/**
 * This is the main Employment page. It simply provides a wrapper for each
 * individual Work Experience item.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export const EmploymentDisplay = () => {
  // Grab al the Work Experiences. (This method automatically filters by
  // selected skills, so we don't have to worry about it
  const allWorkExperienceData = GetAllWorkExperiences()

  sortArrayByObjectField(allWorkExperienceData, "dateFrom", "desc")

  const EmploymentWrapper = styled.div`
    @media screen and (min-width: 48rem) {
      background-image: url(${employmentBackground});
      background-position: bottom right;
      background-repeat: no-repeat;
      background-size: 400px;
      padding-right: 20px;
    }
  `
  return (
    <EmploymentWrapper id="work-experience-wrapper" className="main-content">
      <EmploymentFilter/>
      {
        allWorkExperienceData.map(workExperienceItem => {
          return <WorkExperienceDiv workExperienceId={workExperienceItem.objId}/>
        })
      }
    </EmploymentWrapper>
  )
}