import styled from "styled-components"

import {DefaultDisplay} from "../sections/default/Default"
import {SkillsDisplay} from "../sections/skills/Skills"
import {EmploymentDisplay} from "../sections/employment/Employment"
import {EducationDisplay} from "../sections/education/Education"

import {GetCurrentPage} from "../../store/slices/settings-slice"

/**
 * Content wrapper is the main wrapper div for our content. It reads
 * the currentPage from the store, then displays the appropriate
 * component based on the value of that variable.
 *
 * Since the splash/home page uses completely different CSS, we create
 * two different inner divs, using whichever one is appropriate
 * @returns {JSX.Element}
 * @constructor
 */
export const ContentWrapper = () => {

  // Wrapper for the homepage/splash div.
  const HomePageWrapper = styled.div`
    position: relative;
    display: flex;
  `

  // Wrapper for our real content pages
  const ContentPageWrapper = styled.div`
    background-color: white;
    margin-top: 10px;
    color: black;
    overflow: hidden;
  `

  // Maps the currentPage value to the three things we
  // need to correctly display the content div.
  const pageSectionsMapper = {
    Home: {
      wrapper: HomePageWrapper,
      component: DefaultDisplay,
      title: null
    },
    Skills: {
      wrapper: ContentPageWrapper,
      component: SkillsDisplay,
      title: "Skills"
    },
    Work: {
      wrapper: ContentPageWrapper,
      component: EmploymentDisplay,
      title: "Employment"
    },
    Education: {
      wrapper: ContentPageWrapper,
      component: EducationDisplay,
      title: "Education"
    },
  }

  const currentPage = GetCurrentPage()

  const {wrapper, component, name} = currentPage && pageSectionsMapper[currentPage]
    ? pageSectionsMapper[currentPage]
    : {wrapper: HomePageWrapper, component: DefaultDisplay, name: null}

  const WrapperToUse = wrapper
  const ComponentToDisplay = component
  const sectionName = name

  const SectionTitle = styled.h1`
    padding-left: 20px;
  `

  return (
    <WrapperToUse id="main-panel-wrapper">
      {
        sectionName && <SectionTitle>{sectionName}</SectionTitle>
      }
      <ComponentToDisplay id="main-panel-content"/>
    </WrapperToUse>
  )
}