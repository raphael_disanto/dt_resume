import {configureStore} from '@reduxjs/toolkit'

import userReducer from "./slices/user-slice"
import skillsReducer from "./slices/skills-slice"
import workExperienceReducer from "./slices/work-experience-slice"
import educationReducer from "./slices/education-slice"
import settingsReducer from "./slices/settings-slice"

/**
 * Initializes the store with the fields we need in the app.
 */
export default configureStore({
  reducer: {
    settings: settingsReducer,
    user: userReducer,
    skills: skillsReducer,
    workExperience: workExperienceReducer,
    education: educationReducer,
  }
})