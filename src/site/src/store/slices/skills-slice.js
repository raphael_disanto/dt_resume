import {useSelector} from "react-redux"
import {createSlice} from "@reduxjs/toolkit"

import {Skill, SkillCategory} from "../../objects/skill"

/**
 * Redux Slice that manages our Skills data.
 */
export const skillsSlice = createSlice({
  name: "skills",
  initialState: {
    categories: {}, // Hash of categories, indexed by ID.
    all: {},        // Hash of skills, indexed by ID.
  },
  reducers: {
    storeSkills: (state, data) => {
      for (const item of data.payload) {
        state.all[item["obj_id"]] = new Skill(item)
      }
    },
    storeCategories: (state, data) => {
      for (const item of data.payload) {
        state.categories[item["obj_id"]] = new SkillCategory(item)
      }
    }
  }
})

/**
 * Gets all skill categories as a list
 *
 * @returns [SkillCategory]
 */
export const GetAllSkillCategories = () => {
  const allSkillCategories = useSelector(state => state.skills.categories)
  return Object.values(allSkillCategories)
}

/**
 * Gets all skills as a list
 *
 * @returns [Skill]
 */
export const GetAllSkills = () => {
  const allSkills = useSelector(state => state.skills.all)
  return Object.values(allSkills)
}

/**
 * Gets all skills belonging to a given category ID
 *
 * @param {number} skillCategoryId
 * @returns [Skill]
 */
export const GetSkillsByCategory = (skillCategoryId) => {
  const allSkills = useSelector(state => state.skills.all)
  return Object.values(allSkills).filter(skill => skill.skillCategoryId === skillCategoryId)
}

/**
 * Gets the skill for a given ID, or null if no Skill
 *
 * @param skillId
 * @returns {Skill|null}
 */
export const GetSkillById = (skillId) => {
  const allSkills = useSelector(state => state.skills.all)
  return allSkills[skillId] || null
}

/**
 * Gets the category for a given ID, or null if no Category
 *
 * @param categoryId
 * @returns {SkillCategory|null}
 */
export const GetSkillCategoryById = (categoryId) => {
  const allSkillCategories = useSelector(state => state.skills.categories)
  return allSkillCategories[categoryId] || null
}

export const {storeSkills, storeCategories} = skillsSlice.actions
export default skillsSlice.reducer