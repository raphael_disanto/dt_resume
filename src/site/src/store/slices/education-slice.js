import {createSlice} from "@reduxjs/toolkit"

import {School} from "../../objects/education"

/**
 * Redux Slice that manages our Education data.
 */
export const educationSlice = createSlice({
  name: "education",
  initialState: [],
  reducers: {
    storeEducation: (state, data) => {
      for (const item of data.payload) {
        state.push(new School(item))
      }
    }
  }
})

export const storeEducation = educationSlice.actions.storeEducation
export default educationSlice.reducer