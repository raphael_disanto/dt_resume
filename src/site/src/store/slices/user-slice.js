import {useSelector} from "react-redux"
import {createSlice} from "@reduxjs/toolkit"
import {User} from "../../objects/user"

/**
 * Redux Slice that manages our User data.
 */
export const userSlice = createSlice({
  name: "user",
  initialState: {
    data: {},
  },
  reducers: {
    storeUser: (state, data) => {
      const payload = data.payload
      state.data = new User(payload)
    }
  }
})

/**
 * Simply returns the user data stored in the store.
 *
 * @returns {*}
 * @constructor
 */
export const GetUser = () => {
  const userData = useSelector(state => state.user)
  return userData.data
}

export const storeUser = userSlice.actions.storeUser
export default userSlice.reducer