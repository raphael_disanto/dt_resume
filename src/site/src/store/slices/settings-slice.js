import {createSlice} from "@reduxjs/toolkit"
import {useSelector} from "react-redux"

/**
 * Redux Slice that manages our general settings data.
 */
export const SettingsSlice = createSlice({
  name: "settings",
  initialState: {
    currentPage: "Home",
    filterTerms: [],
    loadingData: true,
  },
  reducers: {
    storeCurrentPage: (state, data) => {
      state.currentPage = data.payload
    },

    storeFilterTerms: (state, data) => {
      state.filterTerms = data.payload
    },
    storeLoadingData: (state, data) => {
      state.loadingData = data.payload
    }
  }
})

/**
 * Gets the current page
 *
 * @returns {string}
 */
export const GetCurrentPage = () => {
  return useSelector(state => state.settings.currentPage)
}

/**
 * Gets the current employment skills filter settings
 *
 * @returns {[string]}
 * @constructor
 */
export const GetFilterTerms = () => {
  return useSelector(state => state.settings.filterTerms)
}

/**
 * Returns the value of the loadingData field
 *
 * @returns {boolean|any}
 * @constructor
 */
export const GetLoadingData = () => {
  return useSelector(state => state.settings.loadingData)
}

export const {storeCurrentPage, storeFilterTerms, storeLoadingData,} = SettingsSlice.actions
export default SettingsSlice.reducer