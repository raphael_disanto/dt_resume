import {useSelector} from "react-redux"
import {createSlice} from "@reduxjs/toolkit"

import {GetFilterTerms} from "./settings-slice"
import {WorkAchievement, WorkExperience} from "../../objects/employment";

/**
 * Redux Slice that manages our Employment data.
 */
export const workExperienceSlice = createSlice({
  name: "workExperience",
  initialState: {
    experiences: {},
    achievements: {},
    filterTerms: [],
  },
  reducers: {
    storeWorkExperience: (state, data) => {
      for (const workExperience of data.payload) {
        state.experiences[workExperience["obj_id"]] = new WorkExperience(workExperience)
      }
    },

    storeWorkAchievements: (state, data) => {
      for (const workAchievement of data.payload) {
        state.achievements[workAchievement["obj_id"]] = new WorkAchievement(workAchievement)
      }
    },
  }
})

/**
 * Gets all work experiences as a list. Since this method uses GetAchievementByExperienceId
 * and that method filters by selected skills, we will exclude any WorkExperiences that have
 * zerp (0) achievements, after filtering.
 *
 * @returns [WorkExperience]
 */
export const GetAllWorkExperiences = () => {
  /** @type {[WorkExperience]} **/
  const allWorkExperiences = useSelector(state => state.workExperience.experiences)
  return Object.values(allWorkExperiences).filter(w => GetAchievementByExperienceId(w.objId).length > 0)
}

/**
 * Gets a single work experience by ID, or null if none exist.
 *
 * @param workExperienceId
 * @returns {WorkExperience|null}
 */
export const GetWorkExperienceById = (workExperienceId) => {
  const allWorkExperiences = useSelector(state => state.workExperience.experiences)
  return allWorkExperiences[workExperienceId] || null
}


/**
 * Gets all work achievements as a list
 *
 * @returns [WorkExperience]
 */
export const GetAllWorkAchievements = () => {
  /** @type {[WorkExperience]} **/
  const allWorkExperiences = useSelector(state => state.workExperience.achievements)
  return Object.values(allWorkExperiences)
}

/**
 * Gets all Work Achievements owned by a single Work Experience, filtered by
 * any search terms from the user.
 *
 * @param workExperienceId
 * @returns [WorkAchievement]
 */
export const GetAchievementByExperienceId = (workExperienceId) => {
  const allWorkAchievements = useSelector(state => state.workExperience.achievements)
  const filterTerms = GetFilterTerms()
  if (filterTerms.length > 0) {
    return Object.values(allWorkAchievements)
      .filter(achievement => achievement.employerId === workExperienceId)
      .filter(achievement => filterTerms.some(term => achievement.skills.includes(term)))
  }
  return Object.values(allWorkAchievements).filter(achievement => achievement.employerId === workExperienceId)
}

/**
 * Gets a single work achievement by ID, or null if none exist
 *
 * @param achievementId
 * @returns {WorkAchievement|null}
 * @constructor
 */
export const GetAchievementById = (achievementId) => {
  const allAchievements = useSelector(state => state.workExperience.achievements)
  return allAchievements[achievementId] || null
}


export const {storeWorkExperience, storeWorkAchievements} = workExperienceSlice.actions
export default workExperienceSlice.reducer