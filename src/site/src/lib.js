import {format} from "date-fns"

/**
 * Worker function that sorts an array of objects by a given field.
 * Optionally sorts by ascending (default) or descending order.
 * @param {[]} arr
 * @param {string} field
 * @param {"asc"|"desc"}direction
 * @returns {[]}
 */
export const sortArrayByObjectField = (arr, field, direction = "asc") => {
  const result = direction === "asc" ? 1 : -1

  // No items? Nothing to filter.
  if (arr.length === 0) return []

  // Make sure this object actually has the field we want, otherwise
  // we'll get an error
  if (!arr[0].hasOwnProperty(field)) return arr

  // Now we can actually sort the items.
  return arr.sort((a, b) => {
    return a[field] > b[field] ? result : result * -1
  })
}

/**
 * Wrapper around the format function from date-fns
 * @param date
 * @param month
 * @returns {string}
 */
export function convertDate(date, month = false) {
  if (date === null) {
    return "Present"
  } else {
    const dateFormat = month === true
      ? ("MMMM, yyyy")
      : ("yyyy")
    return format(date, dateFormat)
  }
}