import React from 'react'
import ReactDOM from 'react-dom/client'
import {Provider} from 'react-redux'
import {createBrowserRouter, RouterProvider} from "react-router-dom"

import store from './store/store'
import {Loader} from "./components/loader/Loader"
import {ErrorPage} from "./components/error/Error";

import './index.css'

const root = ReactDOM.createRoot(document.getElementById('root'))


// Initialize the router with our routes.
const router = createBrowserRouter([
  {
    path: "/",
    element: <Loader pageName="Home" />,
    errorElement: <ErrorPage />
  },
  {
    path: "/home",
    element: <Loader pageName="Home" />,
    errorElement: <ErrorPage />
  },
  {
    path: "/skills",
    element: <Loader pageName="Skills" />,
    errorElement: <ErrorPage />
  },
  {
    path: "/work",
    element: <Loader pageName="Work" />,
    errorElement: <ErrorPage />
  },
  {
    path: "/school",
    element: <Loader pageName="Education" />,
    errorElement: <ErrorPage />
  }
])

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
)

