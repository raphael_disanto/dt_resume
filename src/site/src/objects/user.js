/**
 * @typedef {Object} UserInputPayload
 * @property {number} obj_id
 * @property {string} first_name
 * @property {string} last_name
 * @property {string} tagline
 * @property {string} url
 * @property {string} linkedin
 * @property {string} summary
 *
 */

/**
 * User Object. Contains information about a given user.
 */
export class User {
  /**
   * @param {UserInputPayload} inputData
   */
  constructor(inputData) {
    this.objId = inputData.obj_id || 0
    this.firstName = inputData.first_name || ""
    this.lastName = inputData.last_name || ""
    this.tagline = inputData.tagline || ""
    this.url = inputData.url || ""
    this.linkedin = inputData.linkedin || ""
    this.summary = inputData.summary || ""
  }
}