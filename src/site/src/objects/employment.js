/**
 * @typedef {Object} WorkExperiencePayload
 * @property {number} obj_id
 * @property {string} employer
 * @property {string} job_title
 * @property {string} date_from
 * @property {string} date_to
 */

/**
 * @typedef {Object} WorkAchievementPayload
 * @property {number} obj_id
 * @property {number} employer_id
 * @property {string} description
 * @property {[number]} skills
 */

import {parseISO} from "date-fns";

/**
 * Work Achievement object
 */
export class WorkAchievement {
  /**
   * @param {WorkAchievementPayload} data
   */
  constructor(data) {
    this.objId = data.obj_id
    this.employerId = data.employer_id
    this.description = data.description
    this.skills = data.skills
  }
}

/**
 * Work Experience Object
 */
export class WorkExperience {
  /**
   * @param {WorkExperiencePayload} data
   */
  constructor(data) {
    this.objId = data.obj_id
    this.employer = data.employer
    this.jobTitle = data.job_title
    this.dateFrom = parseISO(data.date_from)
    this.dateTo = parseISO(data.date_to)
  }
}