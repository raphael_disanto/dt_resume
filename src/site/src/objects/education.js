/**
 * @typedef {Object} SchoolInputPayload
 * @property {number} obj_id
 * @property {string} school_name
 * @property {string} course_studied
 * @property {string} date_from
 * @property {string} date_to
 * @property {number} user_id
 *
 */
import {parseISO} from "date-fns";

/**
 * The object representing a School. Dates must be converted from ISO to javascript
 * date objects,
 */
export class School {
  /**
   * @param {SchoolInputPayload} data
   */
  constructor(data) {
    this.objId = data.obj_id
    this.schoolName = data.school_name
    this.courseStudied = data.course_studied
    this.dateFrom = parseISO(data.date_from)
    this.dateTo = parseISO(data.date_to)
  }
}