/**
 * @typedef {Object} SkillInputPayload
 * @property {number} obj_id
 * @property {number} skill_category_id
 * @property {string} skill_name
 */

/**
 * Basic Skill object
 */
export class Skill {
  /**
   * @param {SkillInputPayload} data
   */
  constructor(data) {
    this.objId = data.obj_id
    this.skillCategoryId = data.skill_category_id
    this.skillName = data.skill_name
  }
}

/**
 * @typedef {Object} SkillCategoryInputData
 * @property {number} obj_id
 * @property {string} skill_category_name
 */

/**
 * A Skill Category object.
 */
export class SkillCategory {
  /**
   * @param {SkillCategoryInputData} data
   */
  constructor(data) {
    this.objId = data.obj_id
    this.skillCategoryName = data.skill_category_name
  }
}