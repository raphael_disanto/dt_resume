from helpers.database_connector import DatabaseConnector
from helpers.lib import get_return_data


class API:
  """
  Main working class for the back end API. All endpoints end up in here.
  """
  def __init__(self, db: DatabaseConnector):
    self.db = db

  def get_user(self, user_id: int = 0):
    """
    Gets all data for the user with ID user_id

    :param int user_id:
    :return:
    """
    success = False
    data = {}
    if user_id == 0:
      message = "No user ID provided"
    else:
      query = "SELECT * FROM general_info WHERE obj_id=%(obj_id)s"
      data = {"obj_id": user_id}
      result = self.db.get(query=query, data=data)
      success = result.success
      data = result.data
      message = result.message

    return get_return_data(
      success=success,
      message=message,
      data=data,
    )

  def get_education(self, user_id: int = 0):
    """
    Gets education data for the user with ID user_id
    :param int user_id:
    :return:
    """
    success = False
    data = {}
    if user_id == 0:
      message = "No user ID provided"
    else:
      query = "SELECT * FROM education WHERE user_id=%(user_id)s"
      data = {"user_id": user_id}
      result = self.db.get(query=query, data=data)
      success = result.success
      data = result.data
      message = result.message

    return get_return_data(
      success=success,
      message=message,
      data=data,
    )

  def get_employment(self, user_id: int = 0):
    """
    Returns all Work Experiences for a given user ID

    :param int user_id:
    :return:
    """
    success = False
    data = {}
    if user_id == 0:
      message = "No user ID provided"
    else:
      query = "SELECT * FROM work_experience WHERE user_id=%(user_id)s"
      data = {"user_id": user_id}
      result = self.db.get(query=query, data=data)
      success = result.success
      data = result.data
      message = result.message

    return get_return_data(
      success=success,
      message=message,
      data=data,
    )

  def get_achievements(self, user_id: int = 0):
    """
    Returns all Work Achievements for a given user ID. This includes
    pulling data from the achievement_skills table to append the list
    of skills used in each achievement.

    :param int user_id:
    :return:
    """
    success = False
    data = {}
    if user_id == 0:
      message = "No user ID provided"
    else:
      work_achievement_query = "SELECT * FROM work_achievements WHERE user_id=%(user_id)s"
      work_achievement_data = {"user_id": user_id}
      result = self.db.get(query=work_achievement_query, data=work_achievement_data)
      if result.success:
        for work_achievement in result.data:
          this_achievement_skills = self.get_work_achievement_skills(work_achievement_id=work_achievement["obj_id"])
          work_achievement["skills"] = [achievement_skill["skill_id"] for achievement_skill in this_achievement_skills]
      success = result.success
      data = result.data
      message = result.message

    return get_return_data(
      success=success,
      message=message,
      data=data,
    )

  def get_work_achievement_skills(self, work_achievement_id: int) -> list:
    """
    Returns all the skills involved in a given work achievement. This
    is not a callable endpoint. It's only used by get_achievements

    :param int work_achievement_id:
    :return:
    """
    achievement_skills_query = "SELECT * FROM achievement_skills WHERE achievement_id=%(achievement_id)s"
    achievement_skills_data = {"achievement_id": work_achievement_id}
    achievement_skills_result = self.db.get(
      query=achievement_skills_query,
      data=achievement_skills_data
    )
    if achievement_skills_result.success:
      return achievement_skills_result.data
    else:
      return []

  def get_skill_categories(self, user_id: int = 0):
    """
    Gets all skill categories possessed by this user
    :param int user_id:
    :return:
    """

    success = False
    data = {}
    if user_id == 0:
      message = "No user ID provided"
    else:
      query = "SELECT * FROM skill_categories WHERE user_id=%(user_id)s"
      data = {"user_id": user_id}
      result = self.db.get(query=query, data=data)
      success = result.success
      data = result.data
      message = result.message

    return get_return_data(
      success=success,
      message=message,
      data=data,
    )

  def get_skills(self, user_id: int = 0):
    """
    Gets all skills possessed by this user
    :param int user_id:
    :return:
    """

    success = False
    data = {}
    if user_id == 0:
      message = "No user ID provided"
    else:
      query = "SELECT * FROM skills WHERE user_id=%(user_id)s"
      data = {"user_id": user_id}
      result = self.db.get(query=query, data=data)
      success = result.success
      data = result.data
      message = result.message

    return get_return_data(
      success=success,
      message=message,
      data=data,
    )
