import os

from fastapi import FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware

from api import API
from endpoints import all_endpoints
from helpers.database_connector import DatabaseConnector, DatabaseConfigurationObject
from helpers.lib import do_error, log


app = FastAPI()

# We need to accept requests from the front end, so let's enable CORS
# for that domain.
origins = [
    "http://localhost:13531",
    "http://windsor-county.com:13531",
    "http://dt.windsor-county.com:13531",
    "http://windsor-county.com",
    "http://dt.windsor-county.com",

]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

db_host = "localhost"
db_port = "13533"

if "DB_HOST" in os.environ and os.environ["DB_HOST"]:
    db_host = os.environ["DB_HOST"]
if "DB_PORT" in os.environ and os.environ["DB_PORT"]:
    db_port = os.environ["DB_PORT"]


# Initialize our database connection
db_config = DatabaseConfigurationObject(
    db_host=db_host,
    db_port=db_port,
    db_user="postgres",
    db_pass="docker",
    db_name="dt_resume",
)
db = DatabaseConnector(config=db_config, application_name="DT Resume")
res = db.connect()

if res.success is False:
    log(res.exception)
    do_error(s="Failed to connect")

api_controller = API(db)

# Now we can set up our endpoints.
router = APIRouter()
for endpoint, function_name in all_endpoints.items():
    function = getattr(api_controller, function_name)
    router.add_api_route("/" + endpoint + "/{user_id}", function, methods=["GET"])

app.include_router(router=router)