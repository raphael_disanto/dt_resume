"""
Dictionary mapping the public-facing API endpoints to the method within the API
class that each endpoint should call.
"""

all_endpoints = {
  "user": "get_user",
  "education": "get_education",
  "employment": "get_employment",
  "achievements": "get_achievements",
  "skills": "get_skills",
  "skillCategories": "get_skill_categories",
}
