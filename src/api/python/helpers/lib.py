import os
import datetime
from typing import TypedDict


class ReturnPayload(TypedDict):
    success: bool
    message: str
    data: dict


# Dump an error, exit the process
def do_error(s: str):
    log(s, "ERROR")
    os._exit(1)


def log(s: str, log_type: str = "INFO"):
    """
    Write a message to the console, prepending a timestamp
    :param str s: Message to print
    :param str log_type: Type of the log, prefixed to the output. Defaults to INFO
    :return:
    """
    if s == "":
        print("")
    else:
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(timestamp + " --" + log_type + "-- " + str(s))

def get_return_data(success: bool, message: str, data: dict) -> ReturnPayload:
    """
    Returns a correctly formatted return data payload.
    :param bool success:
    :param str message:
    :param dict data:
    :return:
    """
    return {
        "success": success,
        "message": message,
        "data": data
    }
