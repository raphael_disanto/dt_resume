"""
    * database_connector.py : Database wrapper class for python. Abstracts basic
    * database queries (but maintains parameterization) to provide encapsulated
    * methods for both retrieving data and executing non-data retrieval queries
    * against a PostgreSQL database.)
"""

from helpers.lib import log
import psycopg2
import psycopg2.extras


class DatabaseConnectionResult:
    success = False
    exception = None
    message = ""


class DatabaseReturnObject:
    """
    Class for returning data from the DatabaseConnector wrapper. Provides
    six (6) properties associated with the executed query and returned data:

    1. query        -   String. The original query submitted.
    2. query_params -   Dictionary. The original parameters used in the query
                        (if applicable)
    3. num_records  -   Integer. The number of records returned. Included
                        because some queries can execute successfully but
                        return zero records.
    4. success      -   Boolean. Whether or not the query was successful.
    5. message      -   String. The error message provided by psycopg2 on
                        query failure.
    6. data         -   List of dictionaries. The data returned by the query
                        (if any)

    """
    query = ""
    full_query = ""
    query_params = {}
    num_records = 0
    success = False
    message = ""
    data = []

    def __iter__(self):
        for row in self.data:
            yield row

    def snapshot(self):
        """
        Returns a snapshot of the query, inclding the original query,
        plus any parameters, etc. Mostly used for debugging.
        :return:
        """
        ret_val = """
query: """ + self.query + """
full_query: """ + self.full_query + """
num_records: """ + str(self.num_records) + """
success: """ + str(self.success) + """
message: """ + str(self.message) + """
query_params: """

        if self.query_params is not None and len(self.query_params) > 0:
            for param in self.query_params:
                ret_val += "\n\t"+param+": " + str(self.query_params[param])
        else:
            ret_val += "None"

        if len(self.data) > 0:
            ret_val += """
Data: [list]"""
        else:
            ret_val += """
Data: None"""

        return ret_val


class DatabaseConfigurationObject:
    """
    Class for configuring a database connection. Requires 5 parameters as part
    of the class constructor: hostname, port, database name, username, and
    password

    """

    db_host = ""
    db_port = ""
    db_name = ""
    db_user = ""
    db_pass = ""

    def __init__(self, db_host, db_port, db_name, db_user, db_pass):
        """
        Constructor. Pass all the parameters needed to connect to a database
        in here

        :param str db_host: Hostname of the machine that runs the database
        :param str db_port: Port that the database is listening on
        :param str db_name: Name of the database to use
        :param str db_user: User to connect as
        :param str db_pass: Password for the user
        """
        self.db_host = db_host
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_pass = db_pass

    def snapshot(self):
        return """
db_host: """ + self.db_host + """
db_port: """ + self.db_port + """
db_name: """ + self.db_name + """
db_user: """ + self.db_user + """
db_pass: """ + self.db_pass + """
        """


class DatabaseConnector:
    """
    Database wrapper class. Abstracts basic parameterized database queries and
    returns data via a DatabaseReturnObject class
    """

    connected: bool = False
    db_object = None
    application_name: str = "None"
    config: DatabaseConfigurationObject = None

    def __init__(self, config: DatabaseConfigurationObject, application_name: str = "DT Database Wrapper"):
        self.config = config
        self.application_name = application_name

    def connect(self) -> DatabaseConnectionResult:
        """
        Method that reads the parameters from the configuration object and
        attempts to connect to the database
        :return:
        """

        log(s="Attempting to connect to DB")
        connection_object = DatabaseConnectionResult()

        # First things first, check to make sure we actually do have some
        # parameters in our config
        if self.config.db_host == "":

            connection_object.message = "No hostname specified in the config"
            return connection_object

        if self.config.db_port == "":
            connection_object.message = "No port specified in the config"
            return connection_object

        if self.config.db_name == "":
            connection_object.message = "No database name specified in the config"
            return connection_object

        if self.config.db_user == "":
            connection_object.message = "No username specified in the config"
            return connection_object

        if self.config.db_pass == "":
            connection_object.message = "No password specified in the config"
            return connection_object



        # Now we can try to connect to the database itself.
        try:
            self.db_object = psycopg2.connect(
                host=self.config.db_host,
                port=self.config.db_port,
                user=self.config.db_user,
                password=self.config.db_pass,
                database=self.config.db_name,
                application_name=self.application_name
            )

        # If we get an error, log it.
        except psycopg2.Error as e:
            print(e)
            error_message = "Failed to connect to database!\n"
            error_message += "hostname: " + self.config.db_host + "\n"
            error_message += "port: " + self.config.db_port + "\n"
            error_message += "username: " + self.config.db_user + "\n"
            error_message += "password: " + self.config.db_pass + "\n"
            error_message += "database: " + self.config.db_name + "\n"
            connection_object.message = error_message
            connection_object.exception = e.pgerror
            return connection_object
        else:
            connection_object.success = True
            self.connected = True
            self.db_object.autocommit = False
            return connection_object

    def get(self, query, data=None):
        """
        Wrapper function for returning data. Returns a DatabaseReturnObject
        whose 'data' property contains a list of dictionaries, one for each
        row of data retrieved.

        :param string query: Query to be executed (can be parameterized)
        :param dict data: Dictionary of named parameter/value pairs
        :return DatabaseReturnObject:
        """

        # Initialize our return object
        ret_obj = DatabaseReturnObject()

        if not self.connected:
            ret_obj.message = "Database connection has not been established"
            return ret_obj

        # And set these first, so we have them later if we need them (i.e. if
        # the query fails)
        ret_obj.query = query
        ret_obj.query_params = data

        # Initialize the cursor.
        cursor = self.db_object.cursor(
            cursor_factory=psycopg2.extras.RealDictCursor)

        # Now we can try to execute the query itself.
        try:
            cursor.execute(query, data)

            # If we were successful, set that flag and write the retrieved data
            # to the return object.
            ret_obj.success = True
            if cursor.rowcount > 0:
                ret_obj.num_records = cursor.rowcount
                ret_obj.data = cursor.fetchall()
            # Finally commit the transaction
            self.db_object.commit()

        # If we get an error, dump the message in the return object and rollback
        # the transaction
        except psycopg2.Error as e:
            ret_obj.message = e.pgerror
            ret_obj.full_query = cursor.mogrify(query, data).decode("utf-8")
            self.db_object.rollback()

        cursor.close()
        return ret_obj

    def execute(self, query, data=None):
        """
        Wrapper function for executing a query that does not return data. Still
        returns a DatabaseReturnObject, but only so we can check the success or
        fail status, and query the error message should that be necessary.

        :param str query: Query to be executed (can be parameterized)
        :param dict data: Dictionary of named parameter/value pairs
        :return DatabaseReturnObject:
        """

        # Initialize our return object
        ret_obj = DatabaseReturnObject()

        if not self.connected:
            ret_obj.message = "Database connection has not been established"
            return ret_obj

        # And set these first, so we have them later if we need them (i.e. if
        # the query fails)
        ret_obj.query = query
        ret_obj.query_params = data

        # Initialize the cursor.
        cursor = self.db_object.cursor()

        # Now we can try to execute the query itself.
        try:
            cursor.execute(query, data)

            # If we were successful, set that flat in the return object
            ret_obj.success = True

            # Finally commit the transaction
            self.db_object.commit()

        # If we get an error, dump the message in the return object and rollback
        # the transaction
        except psycopg2.Error as e:
            ret_obj.message = e.pgerror
            ret_obj.full_query = cursor.mogrify(query, data).decode("utf-8")
            self.db_object.rollback()
        except ValueError as e:
            ret_obj.message = str(e)
            ret_obj.full_query = cursor.mogrify(query, data).decode("utf-8")
            self.db_object.rollback()

        cursor.close()
        return ret_obj
