import dateFNS from "date-fns"
const {format} = dateFNS

/**
 * Write a message to the console, prepending a timestamp
 * @param s
 * @param log_type
 */
export const log = (s, log_type="INFO") => {
  if (s === "") {
    console.log("")
  } else {
    const date = format(new Date(), "yyyy-MM-dd HH:mm:ss")
    console.log(date + " --" + log_type + "-- " + s)
  }
}