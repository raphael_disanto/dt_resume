import pg from 'pg'
import {log} from "./lib.mjs";
import {da, tr} from "date-fns/locale";

const {Client} = pg

class DatabaseConnectionResult {
  constructor() {
    this.success = false
    this.exception = null
    this.message = ""
  }
}

class DatabaseReturnObject {
 constructor() {
   this.query = ""
   this.fullQuery = ""
   this.queryParams = {}
   this.numRecords = 0
   this.success = false
   this.message = ""
   this.data = []
 }
}

/**
 * Class for configuring a database connection. Requires 5 parameters as part
 * of the class constructor: hostname, port, database name, username, and
 * password
 */
export class DatabaseConfigurationObject {
  /**
   * @param {String} dbHost
   * @param {String} dbPort
   * @param {String} dbName
   * @param {String} dbUser
   * @param {String} dbPass
   */
  constructor({dbHost, dbPort, dbName, dbUser, dbPass}) {
    this.dbHost = dbHost
    this.dbPort = dbPort
    this.dbName = dbName
    this.dbUser = dbUser
    this.dbPass = dbPass
  }

  snapshot() {
    return `
dbHost: ${this.dbHost}
dbPort: ${this.dbPort}
dbName: ${this.dbName}
dbUser: ${this.dbUser}
dbPass: ${this.dbPass}
    `
  }
}

/**
 * Database wrapper class. Abstracts basic parameterized database queries and
 * returns data via a DatabaseReturnObject class
 */
export class DatabaseConnector {
  /**
   * @param {DatabaseConfigurationObject} dbConfig
   * @param {String} applicationName
   */
  constructor({dbConfig, applicationName = "DT Database Wrapper"}) {
    /** @type {Boolean} */
    this.connected = false
    /** @type {Client} */
    this.dbClient = null
    /** @type {String} */
    this.applicationName = applicationName
    /** @type {DatabaseConfigurationObject} */
    this.config = dbConfig
  }

  async connect() {
    log("Attempting to connect to DB as " + this.applicationName)
    log(this.config.snapshot())
    const res = new DatabaseConnectionResult()

    if (this.config.dbHost === "") {
      res.message = "No hostname specified in the config"
      return res
    }

    if (this.config.dbPort === "") {
      res.message = "No port specified in the config"
      return res
    }

    if (this.config.dbName === "") {
      res.message = "No database specified in the config"
      return res
    }

    if (this.config.dbUser === "") {
      res.message = "No username specified in the config"
      return res
    }

    if (this.config.dbPass === "") {
      res.message = "No password specified in the config"
      return res
    }

    try {
      this.dbClient = new Client({
        user: this.config.dbUser,
        host: this.config.dbHost,
        database: this.config.dbName,
        password: this.config.dbPass,
        port: this.config.dbPort
      })

      await this.dbClient.connect()
      this.connected = true
    } catch (e) {
      console.log(e)
      let errorMessage = "Failed to connect to database!\n"
      errorMessage += "hostname: " + this.config.dbHost + "\n"
      errorMessage += "port: " + this.config.dbPort + "\n"
      errorMessage += "username: " + this.config.dbUser + "\n"
      errorMessage += "password: " + this.config.dbPass + "\n"
      errorMessage += "database: " + this.config.dbName + "\n"
      res.message = errorMessage
      res.exception = e.toString()
      return res
    }
  }

  /**
   *
   * @param query
   * @param params
   * @returns {Promise<DatabaseReturnObject>}
   */
  async get({query, params}) {
    const retObj = new DatabaseReturnObject()
    retObj.query = query
    retObj.queryParams = params

    if (!this.connected) {
      retObj.message = "Database connection has not been established"
      return retObj
    }

    try {
      let data = []
      if (params.length === 0) {
        data = await this.dbClient.query(query)
      } else {
        data = await this.dbClient.query(query, params)
      }
      console.log(data)
    } catch (e) {
      console.log(e)
    }
  }
}