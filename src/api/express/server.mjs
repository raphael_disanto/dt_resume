import express from 'express'
import {DatabaseConfigurationObject, DatabaseConnector} from "./helpers/database_connector.mjs";

const dbHost = process.env["DB_HOST"] || "localhost"
const dbPort = process.env["DB_PORT"] || "13533"

const dbConfig = new DatabaseConfigurationObject({
  dbHost,
  dbPort,
  dbName: "dt_resume",
  dbUser: "postgres",
  dbPass: "docker"
})
const db = new DatabaseConnector({
  dbConfig,
  applicationName: "DT Resume"
})
const res = await db.connect()

const d = db.get("select * from general_info")
console.log(d)
const app = express()
app.get("/", (req, res) => {
  res.send("Hello world")
})

const server = app.listen("13532", () => {
  console.log("Server listening on " + server.address().address + ":" + server.address().port)
})