#!/usr/bin/env bash
set -e

api_type="fastapi"
if [[ "$1" != "" ]]; then
  api_type="$1"
fi
echo "Checking for postgres"

# Log in for user (`-U`) and once logged in execute quit ( `-c \q` )
# If we can't log in, sleep for 1 sec
until PGPASSWORD=docker psql -p "$DB_PORT" -h "$DB_HOST" -U "postgres" -c '\q'; do
  echo "Postgres is unavailable - sleeping"
  sleep 1
done

# And if we've connected above, we can actually start our server
echo "Postgres is up - starting $api_type server"
case "$api_type" in
fastapi)
  uvicorn --port 13532 --host 0.0.0.0 server:app
  ;;
express)
  node --experimental-specifier-resolution=node server.mjs
esac
