# DT Resume App

## Overview
The DT Resume app is a small simple app demonstrating some basic 
React/Python/Postgres interaction. Like many full-stack web 
apps, the front end is a React-driven SPA, which gets its data
from a Python-based API, which reads from a Postgres RDBMS.

This SPA -> API -> Database pattern is very common in today's web
and so I decided to duplicate it in this mini-stack. This is the
first time I've built anything in modern React and FastAPI, so it
was an interesting and educational experience.

## Prerequisites
Docker. That's it. No, seriously. Many modern web apps these days
are containerized, and so I built this app to mimic that pattern.
If you have docker on your machine, `./launch.sh docker` is all
you should need to get this thing up and rolling. Once all the
containers are launched and ready, just visit: http://localhost:13531
to view the site.

## In The Weeds
For the terminally curious - `./launch.sh` has a bunch of options
behind it. Just run `./launch.sh` with no command line arguments 
to see them. It'll let you launch the 3 "pillars" of the site 
(front end, api, database) in a docker compose cluster, shut the 
cluster down, or launch them natively on your machine. If you want 
to do that last part, you'll need to make sure you have python and 
nodejs installed, in addition to docker. (Yes, you'll still need 
docker, even if you're running the app natively, as the database 
only comes dockerized. The source `.sql` files for seeding the 
database are included in `./src/db`, but if you want to load 
them into your own DB, that is out of scope for this document.)

For installation of Python and NodeJS, I recommend asdf
(https://asdf-vm.com/). It's a handy little tool for downloading
and installing and keeping track of different versions of all 
kinds of runtimes, Python and NodeJS being only two of many. I'd
also recommend using a virtual environment with Python. I used
virtualenv (https://virtualenv.pypa.io/en/latest/), but you can
use whichever you wish.

Node, of course, will create its standard `node_modules` directory
under the project folder, so they'll remain specific to this
project. You will, however, have to install http-server
(https://www.npmjs.com/package/http-server).

`npm install -g http-server`



Once you have Python and NodeJS set up and ready to go, you'll 
need to utilize `package.json` and`requirements.py` to install
the required Javascript and Python modules, respectively.

Node:
```shell
cd ./src/site
npm install
```
Python:
```shell
cd ./src/api
pip install -r requirements.txt
```

Then you should be able to run: 

`./launch dev`

which should launch all 3 pillars of the app natively.

## Structural Overview
While the app is split into 3 pillars, I included all 3 codebases
in a single monorepo for ease of distribution. All sourcecode
lives in the `src` top level directory, which contains 3
subdirectories: `api`, `db`, and `site`.

- `api` contains code for the Python API, served by FastAPI.
- `db` contains the seed SQL and loader script for the database.
- `site` contains the code for the React frontend, served by 
http-server.

## Other Directories/Files Of Note:
- `./build-scripts` contains all the shell scripts for building 
and launching the app.
- `./dockerfiles` contains all the dockerfiles for each pillar (for
when you're running in docker compose mode), also the docker
compose YaML file itself.
- `api.sh`, `db.sh`, and `site.sh` all launch the native versions of 
their respective pillars. (This is mostly to aid in development
and debugging.)

Finally, you'll notice after you've run `./launch.sh` for the first
time that a `./build` directory will have been created. This is
where the build scripts place their final production built code. If
you're running natively, this is where each pillar is served from.
If you're running under compose, this is where the source data for 
each docker container is read from.