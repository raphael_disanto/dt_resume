#!/usr/bin/env bash
# This script is just a wrapper around the build and run scripts for the Frontend.
set -e
./build-scripts/build-site.sh "$@"
./build-scripts/run-site.sh "$@"